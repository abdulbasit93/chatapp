import React, {useContext} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {IconButton} from 'react-native-paper';
import HomeScreen from '../screens/HomeScreen';
import AddRoomScreen from '../screens/AddRoomScreen';
import RoomScreen from '../screens/RoomScreen';
import {AuthContext} from './AuthProvider';

import NotificationsListScreen from '../screens/NotificationsListScreen';
import NotificationTestScreen from '../screens/NotificationTestScreen';
import messaging from '@react-native-firebase/messaging';
import {useNavigation} from '@react-navigation/core';

const ChatAppStack = createNativeStackNavigator();
const ModalStack = createNativeStackNavigator();

function ChatApp({route, navigation}) {
  const {logout} = useContext(AuthContext);

  console.log('route params -----> ', route.params);

  // handle on click of push notification in app's background state
  messaging().onNotificationOpenedApp(remoteMessage => {
    console.log(
      'Notification caused app to open from background state:',
      remoteMessage.notification,
    );
    // navigation.navigate('Room', {thread: route.params.thread._id});
    navigation.navigate('NotificationsList');
  });

  // handle on click of push notification in app's quit state
  messaging()
    .getInitialNotification()
    .then(remoteMessage => {
      if (remoteMessage) {
        console.log(
          'Notification caused app to open from quit state: ',
          remoteMessage.notification,
        );
        navigation.navigate('NotificationsList');
        // navigation.navigate('Room', {thread: route.params.thread._id});
      }
    });

  return (
    <ChatAppStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#6646ee',
        },
        headerTintColor: '#ffffff',
        headerTitleStyle: {
          fontSize: 22,
        },
      }}>
      <ChatAppStack.Screen
        name="Home"
        component={HomeScreen}
        options={({navigation}) => ({
          headerRight: () => (
            <>
              <IconButton
                icon="bell"
                size={28}
                color="#ffffff"
                onPress={() => navigation.navigate('NotificationTest')}
              />
              <IconButton
                icon="message-plus"
                size={28}
                color="#ffffff"
                onPress={() => navigation.navigate('AddRoom')}
              />
              <IconButton
                icon="logout"
                size={28}
                color="#ffffff"
                onPress={() => logout()}
              />
            </>
          ),
        })}
      />
      <ChatAppStack.Screen
        name="Room"
        component={RoomScreen}
        options={({route}) => ({
          title: route.params.thread.name,
        })}
      />
      <ChatAppStack.Screen
        name="NotificationsList"
        component={NotificationsListScreen}
        options={{headerShown: false}}
      />
      <ChatAppStack.Screen
        name="NotificationTest"
        component={NotificationTestScreen}
        options={{headerShown: false}}
      />
    </ChatAppStack.Navigator>
  );
}

export default function HomeStack() {
  return (
    <ModalStack.Navigator
      screenOptions={{presentation: 'modal', headerShown: false}}
      mode="modal"
      headerMode="none">
      <ModalStack.Screen name="ChatApp" component={ChatApp} />
      <ModalStack.Screen name="AddRoom" component={AddRoomScreen} />
    </ModalStack.Navigator>
  );
}
