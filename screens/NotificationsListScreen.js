import React, {Component, Fragment} from 'react';
import PushNotification, {Importance} from 'react-native-push-notification';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

export default class NotificationsListScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pushData: [],
    };
  }
  componentDidMount() {
    let self = this;
    PushNotification.configure({
      onRegister: function (token) {
        console.log('TOKEN:', token);
      },

      onNotification: function (notification) {
        console.log('NOTIFICATION:', notification);

        PushNotification.createChannel(
          {
            channelId: 'channel-id',
            channelName: 'My channel',
            channelDescription: 'A channel to categorise your notifications',
            playSound: false,
            soundName: 'default',
            importance: Importance.HIGH,
            vibrate: true,
          },
          created => console.log(`createChannel returned '${created}'`),
        );

        self._addDataToList(notification);
      },

      senderID: '1090501687137',

      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },

      popInitialNotification: true,

      requestPermissions: true,
    });
  }

  _renderItem = ({item}) => (
    <View style={styles.notifItem} key={item.id}>
      <Text style={{color: 'black', marginBottom: 10, fontSize: 19}}>
        {item.title}
      </Text>
      <Text style={{color: 'black', marginBottom: 20, fontSize: 19}}>
        {item.message}
      </Text>
    </View>
  );

  _addDataToList(data) {
    let array = this.state.pushData;
    array.push(data);
    this.setState({
      pushData: array,
    });
    console.log(this.state);
  }

  render() {
    return (
      <Fragment>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <View style={styles.listHeader}>
            <Text style={{color: '#fff', fontSize: 19}}>
              Push Notifications
            </Text>
          </View>
          <View style={styles.body}>
            {this.state.pushData.length != 0 && (
              <FlatList
                data={this.state.pushData}
                renderItem={item => this._renderItem(item)}
                keyExtractor={item => item.id}
                extraData={this.state}
                contentContainerStyle={{paddingBottom: 0}}
              />
            )}
            {this.state.pushData.length == 0 && (
              <View style={styles.noData}>
                <Text style={styles.noDataText}>
                  You don't have any push notification yet. Send some push
                  messages to show it in the list
                </Text>
              </View>
            )}
          </View>
        </SafeAreaView>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  listHeader: {
    backgroundColor: '#6646EE',
    color: '#222',
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    paddingTop: 10,
    color: 'black',
  },
  noData: {
    paddingVertical: 50,
  },
  noDataText: {
    fontSize: 17,
    lineHeight: 25,
    textAlign: 'center',
    color: '#000',
  },
  message: {
    fontSize: 14,
    paddingBottom: 15,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    color: 'black',
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    paddingHorizontal: 15,
    paddingVertical: 15,
    color: 'black',
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: '#000000',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  notifItem: {
    backgroundColor: 'lightblue',
    padding: 15,
    marginBottom: 15,
  },
});
