import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import firestore from '@react-native-firebase/firestore';

const NotificationTestScreen = ({navigation}) => {
  const [text, onChangeText] = useState('');

  const sendNoti = () => {
    firestore()
      .collection('usertoken')
      .get()
      .then(querySnap => {
        const userDevicetoken = querySnap.docs.map(docSnap => {
          return docSnap.data().token;
        });
        console.log('userDevicetoken ---> ', userDevicetoken);

        fetch('https://boiling-badlands-39942.herokuapp.com/send-noti', {
          method: 'post',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            tokens: userDevicetoken,
            title: 'New Message',
            message: text,
          }),
        }).catch(error => {
          console.log('Error sending message: ', error);
          throw error;
        });
      });
  };

  return (
    <View style={styles.container}>
      <Text style={{fontSize: 20, marginBottom: 20}}>
        Notification Testing Screen
      </Text>

      <TextInput
        style={styles.inputBox}
        onChangeText={onChangeText}
        value={text}
        placeholder="Enter a message"
      />

      <TouchableOpacity style={styles.testBtn} onPress={() => sendNoti()}>
        <Text style={styles.btnText}>Test Notification</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.testBtn}
        onPress={() => navigation.navigate('NotificationsList')}>
        <Text style={styles.btnText}>Notifications List</Text>
      </TouchableOpacity>
    </View>
  );
};

export default NotificationTestScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  testBtn: {
    backgroundColor: 'blue',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 30,
    paddingRight: 30,
    marginBottom: 20,
  },
  btnText: {
    color: '#fff',
    fontSize: 18,
  },
  inputBox: {
    borderWidth: 2,
    borderColor: '#000',
    width: 300,
    marginBottom: 20,
    padding: 10,
  },
});
