import React, {useState, useContext} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Title, IconButton} from 'react-native-paper';
import FormInput from '../components/FormInput';
import FormButton from '../components/FormButton';
import {AuthContext} from '../navigation/AuthProvider';
import messaging from '@react-native-firebase/messaging';

export default function SignUpScreen({navigation}) {
  const {register} = useContext(AuthContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  // const [avatar, setAvatar] = useState('');

  /* const uploadAvatar = () => {
    ImageCropPicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      // console.log(image.path);
      setAvatar(image.path);
      console.log('Avatar URL: ', avatar);
    });
  }; */

  return (
    <View style={styles.container}>
      <Title style={styles.titleText}>Register to Chat</Title>
      <FormInput
        labelName="Email"
        value={email}
        autoCapitalize="none"
        onChangeText={userEmail => setEmail(userEmail)}
      />
      <FormInput
        labelName="Password"
        value={password}
        secureTextEntry={true}
        onChangeText={userPassword => setPassword(userPassword)}
      />
      {/* <TouchableOpacity
        onPress={() => uploadAvatar()}
        style={{
          backgroundColor: '#6646ee',
          paddingTop: 15,
          paddingBottom: 15,
          paddingLeft: 25,
          paddingRight: 25,
        }}>
        <Text style={{color: '#fff'}}>Upload Profile Image</Text>
      </TouchableOpacity> */}
      <FormButton
        title="Sign Up"
        modeValue="contained"
        labelStyle={styles.loginButtonLabel}
        onPress={() => register(email, password)}
      />
      <IconButton
        icon="keyboard-backspace"
        size={30}
        style={styles.navButton}
        color="#6646ee"
        onPress={() => navigation.goBack()}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleText: {
    fontSize: 24,
    marginBottom: 10,
    color: '#fff',
  },
  loginButtonLabel: {
    fontSize: 22,
  },
  navButtonText: {
    fontSize: 18,
  },
  navButton: {
    marginTop: 10,
  },
});
